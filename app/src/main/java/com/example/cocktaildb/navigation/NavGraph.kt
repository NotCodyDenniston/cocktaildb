package com.example.cocktaildb.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.example.cocktaildb.view.screens.CategoryDrinks
import com.example.cocktaildb.view.screens.Category
import com.example.cocktaildb.view.screens.Drinks

@Composable
fun NavGraph(
    navController: NavHostController
) {
    NavHost(
        navController = navController,
        startDestination = Screens.CategoryScreen.route
    ) {
        composable(Screens.CategoryScreen.route){
            Category(navigate = {
                navController.navigate(Screens.CategoryDrinksScreen.passCategory(it.strCategory))
            })
        }
        composable(
            Screens.CategoryDrinksScreen.route,
            arguments = listOf(navArgument("encodedUrl"){
                type = NavType.StringType
            })
        ){ entry ->
            CategoryDrinks(
                navigate = {navController.navigate(Screens.DrinksScreen.passCategory(it.strDrink))},
            category = entry.arguments?.getString("encodedUrl"))
        }
        composable(Screens.DrinksScreen.route,
            arguments = listOf(navArgument("encodedUrl"){
                type = NavType.StringType
            })){ entry ->
            Drinks( drink = entry.arguments?.getString("encodedUrl"))
        }
    }
}
