package com.example.cocktaildb.navigation

import java.net.URLEncoder
import java.nio.charset.StandardCharsets


sealed class Screens(val route:String) {
    object CategoryScreen: Screens("category")
    object CategoryDrinksScreen: Screens("category_drinks/{encodedUrl}") {
        fun passCategory(category: String): String {
            return this.route.replace(
                Regex("\\{.*\\}"),
                URLEncoder.encode(category.replace(" ", "_"),
                StandardCharsets.UTF_8.toString())
            )
        }
    }
    object DrinksScreen: Screens("drinks/{encodedUrl}"){
        fun passCategory(category: String): String {
            return this.route.replace(
                Regex("\\{.*\\}"),
                URLEncoder.encode(category.replace(" ", "_"),
                    StandardCharsets.UTF_8.toString())
            )
        }
    }
}