package com.example.cocktaildb.model.remote.dtos.categories

data class CategoryDTO(
    val strCategory: String
)