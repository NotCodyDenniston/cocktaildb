package com.example.cocktaildb.model.local

data class Category (
    val strCategory: String
)