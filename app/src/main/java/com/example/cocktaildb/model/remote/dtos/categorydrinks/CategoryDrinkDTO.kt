package com.example.cocktaildb.model.remote.dtos.categorydrinks

data class CategoryDrinkDTO(
    val idDrink: String,
    val strDrink: String,
    val strDrinkThumb: String
)