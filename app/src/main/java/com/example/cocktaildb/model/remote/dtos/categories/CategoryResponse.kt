package com.example.cocktaildb.model.remote.dtos.categories

data class CategoryResponse(
    val drinks: List<CategoryDTO>
)