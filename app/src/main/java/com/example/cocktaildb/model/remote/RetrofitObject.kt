package com.example.cocktaildb.model.remote

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET

object RetrofitObject {

    const val BASE_URL = "https://www.thecocktaildb.com/api/json/v1/1/"

    val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun getCocktailService(): CocktailService = retrofit.create()
}