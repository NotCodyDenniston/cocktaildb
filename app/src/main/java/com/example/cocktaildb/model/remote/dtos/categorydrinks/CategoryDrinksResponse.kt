package com.example.cocktaildb.model.remote.dtos.categorydrinks

data class CategoryDrinksResponse(
    val drinks: List<CategoryDrinkDTO>
)