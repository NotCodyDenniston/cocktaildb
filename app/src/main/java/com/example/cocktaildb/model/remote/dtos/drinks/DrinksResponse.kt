package com.example.cocktaildb.model.remote.dtos.drinks

data class DrinksResponse(
    val drinks: List<DrinkDTO>
)