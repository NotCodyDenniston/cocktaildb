package com.example.cocktaildb.model

import com.example.cocktaildb.model.local.Category
import com.example.cocktaildb.model.local.CategoryDrinks
import com.example.cocktaildb.model.local.Drink
import com.example.cocktaildb.model.mapper.CategoryDrinksMapper
import com.example.cocktaildb.model.mapper.CategoryMapper
import com.example.cocktaildb.model.mapper.DrinkMapper
import com.example.cocktaildb.model.remote.CocktailService
import com.example.cocktaildb.model.remote.RetrofitObject
import com.example.cocktaildb.model.remote.dtos.categories.CategoryDTO
import com.example.cocktaildb.model.remote.dtos.categories.CategoryResponse
import com.example.cocktaildb.model.remote.dtos.categorydrinks.CategoryDrinksResponse
import com.example.cocktaildb.model.remote.dtos.drinks.DrinksResponse
import com.example.cocktaildb.util.Resource
import java.util.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import retrofit2.http.Path

object CocktailRepo {
    private val service: CocktailService by lazy { RetrofitObject.getCocktailService() }
    private val mapper: CategoryMapper = CategoryMapper()
    private val categoryMapper: CategoryDrinksMapper = CategoryDrinksMapper()
    private val drinksMapper: DrinkMapper = DrinkMapper()

    suspend fun getCategories(): Resource<List<Category>> = withContext(Dispatchers.IO){
        val fetchResponse: Response<CategoryResponse> =
            service.getAllCategories()

        return@withContext if (fetchResponse.isSuccessful && fetchResponse.body() != null){
            val categoryResponse = fetchResponse.body()!!
            val categoryList: List<Category> = categoryResponse.drinks
                .map { mapper(it) }
            Resource.Success(categoryList)
        } else {
            Resource.Error(fetchResponse.message())
        }
    }

    suspend fun getCategoryDrinks(c: String?): Resource<List<CategoryDrinks>> = withContext(Dispatchers.IO){
        val fetchResponse: Response<CategoryDrinksResponse> =
            service.getCategoryDrinks(c = c)

        return@withContext if (fetchResponse.isSuccessful && fetchResponse.body() != null){
            val drinksInCategoryResponse =
                fetchResponse.body()?.drinks ?: emptyList()
            Resource.Success(
                drinksInCategoryResponse.map { categoryMapper(it) }
            )
        } else {
            Resource.Error(fetchResponse.message())
        }
    }

    suspend fun getDrinks(s: String?): Resource<List<Drink>> = withContext(Dispatchers.IO){
        val fetchResponse: Response<DrinksResponse> =
            service.getDrink(s=s)

        return@withContext if (fetchResponse.isSuccessful && fetchResponse.body() != null){
            val drinksResponse =
                fetchResponse.body()?.drinks ?: emptyList()
            Resource.Success(
                drinksResponse.map { drinksMapper(it) }
            )
        } else {
            Resource.Error(fetchResponse.message())
        }
    }
}