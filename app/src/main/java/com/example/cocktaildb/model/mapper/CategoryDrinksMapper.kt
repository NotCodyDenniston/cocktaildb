package com.example.cocktaildb.model.mapper

import com.example.cocktaildb.model.local.CategoryDrinks
import com.example.cocktaildb.model.remote.dtos.categorydrinks.CategoryDrinkDTO

class CategoryDrinksMapper:Mapper<CategoryDrinkDTO, CategoryDrinks> {
    override fun invoke(dto: CategoryDrinkDTO): CategoryDrinks = with(dto){
        return CategoryDrinks(
            idDrink,
            strDrink,
            strDrinkThumb
        )
    }
}