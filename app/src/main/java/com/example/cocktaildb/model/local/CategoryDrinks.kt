package com.example.cocktaildb.model.local

data class CategoryDrinks (
    val idDrink: String,
    val strDrink: String,
    val strDrinkThumb: String
)