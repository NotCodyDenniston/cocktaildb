package com.example.cocktaildb.model.mapper

import com.example.cocktaildb.model.local.Category
import com.example.cocktaildb.model.remote.dtos.categories.CategoryDTO

class CategoryMapper:Mapper<CategoryDTO, Category> {
    override fun invoke(dto: CategoryDTO): Category = with(dto){
        Category(
            strCategory
        )
    }
}