package com.example.cocktaildb.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktaildb.model.CocktailRepo
import com.example.cocktaildb.model.local.Category
import com.example.cocktaildb.util.Resource
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class CategoryViewModel: ViewModel() {

    private val repo = CocktailRepo

    private val _categories: MutableStateFlow<Resource<List<Category>>> =
        MutableStateFlow(Resource.Loading())
     val categoryState: StateFlow<Resource<List<Category>>>
        get() = _categories

    init {
        getCategories()
    }

    private fun getCategories() = viewModelScope.launch {
        _categories.value = repo.getCategories()
    }
}