package com.example.cocktaildb.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktaildb.model.CocktailRepo
import com.example.cocktaildb.model.local.CategoryDrinks
import com.example.cocktaildb.util.Resource
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class CategoryDrinksViewModel: ViewModel() {

    private val repo = CocktailRepo

    private val _categoryDrinks: MutableStateFlow<Resource<List<CategoryDrinks>>> =
        MutableStateFlow(Resource.Loading())
    val categoryDrinkState: StateFlow<Resource<List<CategoryDrinks>>>
        get() = _categoryDrinks


     fun getCategoryDrinks(c:String?) = viewModelScope.launch {
        _categoryDrinks.value = repo.getCategoryDrinks(c = c)
    }
}