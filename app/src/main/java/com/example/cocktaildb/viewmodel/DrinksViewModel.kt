package com.example.cocktaildb.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktaildb.model.CocktailRepo
import com.example.cocktaildb.model.local.Category
import com.example.cocktaildb.model.local.Drink
import com.example.cocktaildb.util.Resource
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class DrinksViewModel: ViewModel() {

    private val repo = CocktailRepo

    private val _drinks: MutableStateFlow<Resource<List<Drink>>> =
        MutableStateFlow(Resource.Loading())
    val drinkState: StateFlow<Resource<List<Drink>>>
        get() = _drinks



    fun getDrinks(s:String?) = viewModelScope.launch {
        _drinks.value = repo.getDrinks(s = s)
    }
}