package com.example.cocktaildb.view.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.cocktaildb.model.CocktailRepo.getCategoryDrinks
import com.example.cocktaildb.model.local.Category
import com.example.cocktaildb.model.local.CategoryDrinks
import com.example.cocktaildb.model.local.Drink
import com.example.cocktaildb.util.Resource
import com.example.cocktaildb.viewmodel.CategoryDrinksViewModel

@Composable
fun CategoryDrinks(
    categoryDrinksViewModel: CategoryDrinksViewModel = viewModel(),
    navigate: (CategoryDrinks: CategoryDrinks)->Unit,
    category: String?
    ){

    categoryDrinksViewModel.getCategoryDrinks(category)
    when(val categorydrinkState = categoryDrinksViewModel.categoryDrinkState.collectAsState().value){
        is Resource.Error -> {

        }
        is Resource.Loading -> {

        }
        is Resource.Success -> {
            ShowItems(data = categorydrinkState.data, navigate = navigate)
        }
    }



}

@Composable
fun ShowItems(
    data: List<CategoryDrinks>,
    navigate: (CategoryDrinks: CategoryDrinks) -> Unit
){
    Column() {
    Text(text = "WELCOME TO THE CATEGORY OF DRINKS.... MWAHAHAHA👻")
    LazyColumn(){
        items(data){drinkCategory: CategoryDrinks ->
            Button(onClick = { navigate(drinkCategory) }) {

           Text(text = drinkCategory.strDrink)
            }
        }
    }

    }

}