package com.example.cocktaildb.view.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.cocktaildb.model.local.CategoryDrinks
import com.example.cocktaildb.model.local.Drink
import com.example.cocktaildb.util.Resource
import com.example.cocktaildb.viewmodel.CategoryDrinksViewModel
import com.example.cocktaildb.viewmodel.DrinksViewModel

@Composable
fun Drinks(
    drinksViewModel: DrinksViewModel = viewModel(),
    drink: String?
){

    drinksViewModel.getDrinks(s =drink)
    when(val drinkState = drinksViewModel.drinkState.collectAsState().value){
        is Resource.Error -> {

        }
        is Resource.Loading -> {

        }
        is Resource.Success -> {
            ShowItems(data = drinkState.data)
        }
    }



}

@Composable
fun ShowItems(
    data: List<Drink>,

){
    Column(verticalArrangement = Arrangement.Center,
    horizontalAlignment = Alignment.CenterHorizontally) {
        Text(text = "WELCOME TO THE DRINKS🥂.... MWAHAHAHA👻")
        LazyColumn(){
            items(data){drink  ->
                Text(text = drink.strDrink)
            }
        }

    }

}