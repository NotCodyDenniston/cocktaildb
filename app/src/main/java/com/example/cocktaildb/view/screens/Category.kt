package com.example.cocktaildb.view.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.cocktaildb.model.local.Category
import com.example.cocktaildb.model.local.CategoryDrinks
import com.example.cocktaildb.util.Resource
import com.example.cocktaildb.viewmodel.CategoryViewModel

@Composable
fun Category(
    navigate: (Category:Category)->Unit,
    categoryViewModel: CategoryViewModel = viewModel()
){
    when(val categoryState = categoryViewModel.categoryState.collectAsState().value){
        is Resource.Error -> {

        }
        is Resource.Loading -> {

        }
        is Resource.Success -> {
            ShowItems(navigate = navigate, data = categoryState.data)
        }
    }


}
    @Composable
    fun ShowItems(
        data: List<Category>,
        navigate: (Category: Category)->Unit
    ){
        Column(
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

        Text(text = "Welcome to the Category Screen")
    LazyColumn(verticalArrangement = Arrangement.Center,
    horizontalAlignment = Alignment.CenterHorizontally) {

        items(data){ category: Category ->
            Button(onClick = {navigate(category)}){
                Text(text = category.strCategory)
            }
    }
    }

        }
}
